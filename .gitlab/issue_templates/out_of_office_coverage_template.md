<!-- title: {Team Member} OOO from Jun 18 - Jun 28 -->

I'll be on PTO from YYYY-MM-DD to YYYY-MM-DD. Here's a list of everything I'm currently working on and its status.

| What? | Link | DRI | Notes |
|-------|-------|----|-------|
| ... | ... | ... | ... |
| ... | ... | ... | ... |

## Checklist

| Description | Completed |
| ------ | ------ |
| Re-assigned or communicated open work which requires coverage | ✅ |
| FYI in recent Sync meeting | ✅ |
| Time Off by Deel | ✅ |
| Updated GitLab Profile to :palm_tree:  | ✅ |


## Additional Notes

<!-- Anything that you think is important for others to know about that is on your head --> 

<!-- Optional: if you prefer, you can make this issue confidential to members of `gitlab-org` using the quick action: --> 
<!-- /confidential -->

/label ~"type::ignore" ~"group::product analytics"
