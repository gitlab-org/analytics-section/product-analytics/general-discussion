# Issue Opening Tasks

1. [ ] - Update Title to xx.x milestone planning for Product Analytics
1. [ ] - Assign to group EM/PM
1. [ ] - Set milestone to xx.xx
1. [ ] - Update the milestone dates from [this page](https://about.gitlab.com/releases/)
1. [ ] - Delete these opening instructions


> _This issue and linked pages contain information related to upcoming products, features, and functionality.  It is important to note that the information presented is for informational purposes only. Please do not rely on this information for purchasing or planning purposes.  As with all projects, the items mentioned in this video and linked pages are subject to change or delay. The development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc._


**Table of Contents**

[[_TOC_]]

## Boards

- [Build Board](https://gitlab.com/groups/gitlab-org/-/boards/4797884?milestone_title=Started&label_name[]=group%3A%3Aproduct%20analytics)

## Capacity notes

- Milestone runs from **_fill in_** to **_fill in_**
- To calculate available capacity: in a typical milestone of 20 working days, if you’re on PTO for 2 of them, you’d have 90% available capacity, e.g.,  `(1 - (2 / 20)) * 100 = 90%` 

### Capacity by team member

* @dennis - 100%
* _fill in_

Total capacity: :question: 

~frontend - :question:  / ~backend - :question: 

## Theme & Objectives

**Milestone Theme:**

**Milestone objectives:**
1. :mag_right: _fill in_
1. :mag_right: _fill in_
1. :mag_right: _fill in_

### Product prioritized ~"type::feature"  list

1. Any previous release carry-over
    1. :mag_right: _fill in_

1. New feature work
   1. :mag_right: _fill in_   
   1. :mag_right: _fill in_

#### Prioritized Design and Research list

1. Design work
    1. :mag_right: _fill in_

1. Research work
    1. :mag_right: _fill in_

#### Planning breakdown expectations
1. :mag_right: _fill in_


#### Nice to have tasks
1. :mag_right: _fill in_


### Engineering prioritized ~"type::maintenance"  list

1. Any previous release carry-over
    1. :mag_right: _fill in_


1. New maintenance work
    1. :mag_right: _fill in_

### Quality prioritized ~"type::bug"  list

Any previous release carry-over

| LINKED_ISSUE_TITLE                                                                                                     | BUG_AGE | SEVERITY_TAG | PRIORITY_TAG | WEIGHT |
|------------------------------------------------------------------------------------------------------------------------|---------|--------------|--------------|--------------|
| _fill in_ |_fill in_ | _fill in_ | _fill in_ | _fill in_ |

1. New bugs work
 
| LINKED_ISSUE_TITLE                                                                                                     | BUG_AGE | SEVERITY_TAG | PRIORITY_TAG |
|------------------------------------------------------------------------------------------------------------------------|---------|--------------|--------------|
| _fill in_ |_fill in_ | _fill in_ | _fill in_ |


### Deferred Items

Consider moving to following iterations

* _fill in_

## Release Post Items

| Status | Issue | Release Post MR |
| ------ | ------ | ------ |

/label ~devops::analyze "~group::product analytics" ~type::ignore ~"Planning Issue" ~"Category:Product Analytics Visualization" ~"Category:Product Analytics Data Management"
